package io.ibensalem;

public class Test {

	static class Client {
		String nom;
		String prenom;

		public Client(String nom, String prenom) {

			this.nom = nom;
			this.prenom = prenom;
		}

	}

	static class Commande {
		String numero;
		Client client;

		public Commande(String numero, Client client) {

			this.numero = numero;
			this.client = client;
		}

	}

	static class Child extends Client {

		public Child(String nom, String prenom) {
			super(nom, prenom);

		}

	}

	public static void main(String[] args) {
		Client client1 = new Client("test1", "test1");
	}

}
