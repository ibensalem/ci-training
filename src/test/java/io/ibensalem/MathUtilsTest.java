package io.ibensalem;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MathUtilsTest {
	MathUtils mathUtils;

	@BeforeEach
	public void init() {
		mathUtils = new MathUtils();
	}

	@DisplayName("Test Addition method")
	@Test
	void testAddition() {
		assertEquals(4, mathUtils.addition(2, 2), "ERROR : the result must be a 4 not " + mathUtils.addition(2, 2));
	}

	@DisplayName("Test Multiplication method")
	@Test
	void testMultiplication() {
		assertEquals(4, mathUtils.multiplication(2, 2), "ERROR : the result must be a 4 not " + mathUtils.addition(2, 2));
	}

	@DisplayName("Test Devision method")
	@Test
	void testDevision() {
		assertAll( 
			() -> assertEquals(4, mathUtils.addition(2, 2), "ERROR : the result must be a 4 not " + mathUtils.addition(2, 2)),
			() -> assertThrows(ArithmeticException.class,() -> mathUtils.devision(2,0),"ERROR : The division by 0 is impossible")

		
		);
	}

	@DisplayName("Test Subtraction method")
	@Test
	void testSubtraction() {
		assertEquals(0, mathUtils.subtraction(2, 2),
				"ERROR : the result must be a 0 not " + mathUtils.subtraction(2, 2));

	}

}
